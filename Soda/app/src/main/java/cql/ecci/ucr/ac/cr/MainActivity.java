package cql.ecci.ucr.ac.cr;

import androidx.appcompat.app.AppCompatActivity;
import android.net.Uri;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //Métodos para que funcionen los mensajes
    public final static String EXTRA_MESSAGE = "persona";
    private final static int SECOND_ACTIVITY_RESULT_CODE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Instancia de las clases
        final Persona miPersona = new Persona();
        final Soda miSoda = new Soda();

        //Instancia de los botones
        TextView persona = (TextView)findViewById(R.id.persona);
        Button buttonWeb = (Button) findViewById(R.id.buttonWeb);
        Button buttonCall = (Button) findViewById(R.id.buttonCall);
        Button buttonEmail = (Button) findViewById(R.id.buttonEmail);
        Button buttonMap = (Button) findViewById(R.id.buttonMap);
        Button buttonBill = (Button) findViewById(R.id.buttonBill);
        Button buttonSodas = (Button) findViewById(R.id.buttonSodas);

        persona.setText(miPersona.toString() + ": es un placer servirle.");

        buttonWeb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                irPagina(miSoda);
            }
        });
        buttonEmail.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                enviarCorreo(miSoda);
            }
        });

        buttonCall.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                realizarLlamada(miSoda);
            }
        });

        buttonMap.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                irMapa(miSoda);
            }
        });
        buttonBill.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                irCalculadoraPropina(miPersona, miSoda);
            }
        });
        buttonSodas.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                irListaSodas(miSoda);
            }
        });
    }

    //Declaración de los métodos*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*

    private void irPagina(Soda soda){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(soda.getWebsite()));
        startActivity(intent);
    }

    private void realizarLlamada(Soda soda){
        //Abrir el teclado de llamada
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + soda.getTelefono()));
        startActivity(intent);
    }

    private void enviarCorreo(Soda soda){
        //Abrir el correo. Así se hace un correo
        String[] TO = {soda.getEmail()};
        Uri uri = Uri.parse("mailto:" + soda.getEmail())
                .buildUpon()
                .appendQueryParameter("subject", "Consulta Soda Universitaria")
                .appendQueryParameter("body", "Enviado desde SodaUniversitaria.")
                .build();
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, uri);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        startActivity(Intent.createChooser(emailIntent, "Enviar vía correo"));
    }

    private void irMapa(Soda soda){
        //Localización en el mapa. Así se hace un mapa
        String url = "geo:" + String.valueOf(soda.getLongitud()) + "," +
                String.valueOf(soda.getLatitud());
        String q = "?q="+ String.valueOf(soda.getLongitud()) + "," +
                String.valueOf(soda.getLatitud()) + "(" + soda.getNombre() + ")";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url + q));
        intent.setPackage("com.google.android.apps.maps"); startActivity(intent);
    }

    private void irCalculadoraPropina(Persona persona, Soda soda){
        Intent intent = new Intent(this, CalculadoraActivity.class);
        intent.putExtra(EXTRA_MESSAGE, persona);
        // Deseo recibir una respuesta: startActivityForResult()
        startActivityForResult(intent, SECOND_ACTIVITY_RESULT_CODE);
    }

    // El método se llama cuando la segunda actividad termina
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
// check that it is the SecondActivity with an OK result
        if (requestCode == SECOND_ACTIVITY_RESULT_CODE) {
            if (resultCode == RESULT_OK) { // Obtener datos del Intent
                String returnString = data.getStringExtra("montoStr");
                // mostrar la respuesta
                Toast.makeText(getApplicationContext(), returnString, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void irListaSodas(Soda soda){
        Intent intent = new Intent(this, ListaSodasActivity.class);
        startActivity(intent);
    }

}
