package cql.ecci.ucr.ac.cr;

import android.os.Parcel;
import android.os.Parcelable;

public class Persona implements Parcelable {
    private String identificacion;
    private String correo;
    private String nombre;
    private String primerApellido;
    private String segundoApellido;
    private String celular;

    public Persona(String identificacion, String correo, String nombre, String primerApellido, String segundoApellido, String celular){
        this.identificacion = identificacion;
        this.correo = correo;
        this.nombre = nombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.celular = celular;
    }

    public Persona(){
        this.identificacion = "B31494";
        this.correo = "luisda7cr@gmail.com";
        this.nombre = "Luis";
        this.primerApellido = "Carvajal";
        this.segundoApellido = "Rojas";
        this.celular = "88328939";
    }

    protected Persona(Parcel in) {
        setIdentificacion(in.readString());
        setCorreo(in.readString());
        setNombre(in.readString());
        setPrimerApellido(in.readString());
        setSegundoApellido(in.readString());
        setCelular(in.readString());
    }

    public static Creator<Persona> getCREATOR() {
        return CREATOR;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getIdentificacion());
        dest.writeString(getCorreo());
        dest.writeString(getNombre());
        dest.writeString(getPrimerApellido());
        dest.writeString(getSegundoApellido());
        dest.writeString(getCelular());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Persona> CREATOR = new Creator<Persona>() {
        @Override
        public Persona createFromParcel(Parcel in) {
            return new Persona(in);
        }

        @Override
        public Persona[] newArray(int size) {
            return new Persona[size];
        }
    };

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }
    @Override
    public String toString(){
        return nombre + " " + primerApellido + " " + segundoApellido;
    }
}
